#+TITLE: April 8-23, 2020 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2020-04-08 Wed>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-sandeepk: 1
  :END:
** akshay196
*** DONE Introduction to Operating Systems - Part IV
    CLOSED: [2020-04-16 Thu 16:13]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   9.00
    :OWNER: akshay196
    :ID: READ.1580485531
    :TASKID: READ.1580485531
    :END:
    :LOGBOOK:
    CLOCK: [2020-04-16 Thu 15:45]--[2020-04-16 Thu 16:13] =>  0:28
    CLOCK: [2020-04-16 Thu 08:37]--[2020-04-16 Thu 09:23] =>  0:46
    CLOCK: [2020-04-15 Wed 08:02]--[2020-04-15 Wed 09:24] =>  1:22
    CLOCK: [2020-04-14 Tue 08:32]--[2020-04-14 Tue 09:47] =>  1:15
    CLOCK: [2020-04-13 Mon 09:04]--[2020-04-13 Mon 10:20] =>  1:16
    CLOCK: [2020-04-12 Sun 07:17]--[2020-04-12 Sun 08:41] =>  1:24
    CLOCK: [2020-04-11 Sat 09:36]--[2020-04-11 Sat 12:05] =>  2:29
    :END:
    https://classroom.udacity.com/courses/ud923
    - [X] Distributed Shared Memory             (180 min)
    - [X] Datacenter Technologies               (120 min)
    - [X] Sample Final Question                 (180 min)
*** DONE Case Studies in Operating System Concepts
    CLOSED: [2020-04-23 Thu 18:13]
    :PROPERTIES:
    :ESTIMATED:  8
    :ACTUAL:   8.30
    :OWNER: akshay196
    :ID: READ.1580489168
    :TASKID: READ.1580489168
    :END:
    :LOGBOOK:
    CLOCK: [2020-04-23 Thu 17:23]--[2020-04-23 Thu 18:13] =>  0:50
    CLOCK: [2020-04-23 Thu 08:19]--[2020-04-23 Thu 09:04] =>  0:45
    CLOCK: [2020-04-22 Wed 09:27]--[2020-04-22 Wed 11:01] =>  1:34
    CLOCK: [2020-04-21 Tue 09:17]--[2020-04-21 Tue 09:44] =>  0:27
    CLOCK: [2020-04-21 Tue 08:38]--[2020-04-21 Tue 09:07] =>  0:29
    CLOCK: [2020-04-20 Mon 07:37]--[2020-04-20 Mon 08:48] =>  1:11
    CLOCK: [2020-04-19 Sun 11:14]--[2020-04-19 Sun 12:24] =>  1:10
    CLOCK: [2020-04-18 Sat 17:46]--[2020-04-18 Sat 18:45] =>  0:59
    CLOCK: [2020-04-17 Fri 08:11]--[2020-04-17 Fri 09:04] =>  0:53
    :END:
    - [X] The Linux System
    - [ ] Windows 10

** bhavin192
*** DONE Attend The April Emacs MeetUp (Remote!!)
    CLOSED: [2020-04-08 Wed 19:40]
    :PROPERTIES:
    :ESTIMATED: 1.5
    :ACTUAL:   1.70
    :OWNER:    bhavin192
    :ID:       EVENT.1586698794
    :TASKID:   EVENT.1586698794
    :END:
    :LOGBOOK:
    CLOCK: [2020-04-08 Wed 17:58]--[2020-04-08 Wed 19:40] =>  1:42
    :END:
*** DONE Write blog post on live streaming a meetup event
    CLOSED: [2020-04-15 Wed 22:50]
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   3.15
    :OWNER:    bhavin192
    :ID:       WRITE.1586698918
    :TASKID:   WRITE.1586698918
    :END:
    :LOGBOOK:
    CLOCK: [2020-04-15 Wed 22:18]--[2020-04-15 Wed 22:50] =>  0:32
    CLOCK: [2020-04-14 Tue 22:23]--[2020-04-14 Tue 23:00] =>  0:37
    CLOCK: [2020-04-14 Tue 21:55]--[2020-04-14 Tue 22:16] =>  0:21
    CLOCK: [2020-04-14 Tue 21:09]--[2020-04-14 Tue 21:18] =>  0:09
    CLOCK: [2020-04-13 Mon 21:43]--[2020-04-13 Mon 22:21] =>  0:38
    CLOCK: [2020-04-13 Mon 20:25]--[2020-04-13 Mon 21:01] =>  0:36
    CLOCK: [2020-04-12 Sun 21:14]--[2020-04-12 Sun 21:30] =>  0:16
    :END:
*** DONE Mastering Distributed Tracing - III. Getting Value from Tracing [4/4]
    CLOSED: [2020-04-23 Thu 21:04]
    :PROPERTIES:
    :ESTIMATED: 6.5
    :ACTUAL:   10.05
    :OWNER:    bhavin192
    :ID:       READ.1562555265
    :TASKID:   READ.1562555265
    :END:
    :LOGBOOK:
    CLOCK: [2020-04-23 Thu 20:42]--[2020-04-23 Thu 21:04] =>  0:22
    CLOCK: [2020-04-22 Wed 22:15]--[2020-04-22 Wed 23:51] =>  1:36
    CLOCK: [2020-04-22 Wed 20:57]--[2020-04-22 Wed 21:19] =>  0:22
    CLOCK: [2020-04-21 Tue 22:16]--[2020-04-21 Tue 23:10] =>  0:54
    CLOCK: [2020-04-21 Tue 00:05]--[2020-04-21 Tue 00:32] =>  0:27
    CLOCK: [2020-04-20 Mon 22:07]--[2020-04-20 Mon 22:26] =>  0:19
    CLOCK: [2020-04-20 Mon 20:42]--[2020-04-20 Mon 21:16] =>  0:34
    CLOCK: [2020-04-19 Sun 23:16]--[2020-04-19 Sun 23:41] =>  0:25
    CLOCK: [2020-04-19 Sun 21:33]--[2020-04-19 Sun 22:46] =>  1:13
    CLOCK: [2020-04-19 Sun 18:28]--[2020-04-19 Sun 19:08] =>  0:40
    CLOCK: [2020-04-19 Sun 17:59]--[2020-04-19 Sun 18:15] =>  0:16
    CLOCK: [2020-04-19 Sun 16:19]--[2020-04-19 Sun 17:18] =>  0:59
    CLOCK: [2020-04-19 Sun 13:15]--[2020-04-19 Sun 14:17] =>  1:02
    CLOCK: [2020-04-18 Sat 21:08]--[2020-04-18 Sat 21:31] =>  0:23
    CLOCK: [2020-04-16 Thu 23:21]--[2020-04-16 Thu 23:52] =>  0:31
    :END:
    - [X]  9. Turning the Lights On                     (1.5h)
    - [X] 10. Distributed Context Propagation           (  1h)
    - [X] 11. Integration with Metrics and Logs         (  2h)
    - [X] 12. Gathering Insights with Data Mining       (  2h)

** gandalfdwite
*** DONE Terraform: Up & Running by Yevgeniy Brikman - Part II [3/3]
    CLOSED: [2020-04-21 Tue 19:14]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   8.70
    :OWNER: gandalfdwite
    :ID: READ.1585545794
    :TASKID: READ.1585545794
    :END:
    :LOGBOOK:
    CLOCK: [2020-04-21 Tue 16:10]--[2020-04-21 Tue 17:26] =>  1:16
    CLOCK: [2020-04-20 Mon 12:44]--[2020-04-20 Mon 14:06] =>  1:22
    CLOCK: [2020-04-19 Sun 11:05]--[2020-04-19 Sun 13:02] =>  1:57
    CLOCK: [2020-04-17 Fri 21:25]--[2020-04-17 Fri 22:30] =>  1:05
    CLOCK: [2020-04-13 Mon 19:45]--[2020-04-13 Mon 21:10] =>  1:25
    CLOCK: [2020-04-10 Fri 12:53]--[2020-04-10 Fri 14:30] =>  1:37
    :END:
    - [X] How to test Terraform Code        ( 3h )
    - [X] How to use Terraform as team      ( 3h )
    - [X] Future Readings                   ( 2h )

*** DONE Watch FOSDEM-20 Videos [8/8]
    CLOSED: [2020-04-16 Thu 19:09]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   8.82
    :OWNER: gandalfdwite
    :ID: EVENT.1586269186
    :TASKID: EVENT.1586269186
    :END:
    :LOGBOOK:
    CLOCK: [2020-04-16 Thu 17:40]--[2020-04-16 Thu 19:09] =>  1:29
    CLOCK: [2020-04-15 Wed 22:15]--[2020-04-15 Wed 23:21] =>  1:06
    CLOCK: [2020-04-14 Tue 21:49]--[2020-04-14 Tue 23:29] =>  1:40
    CLOCK: [2020-04-13 Mon 15:09]--[2020-04-13 Mon 16:20] =>  1:11
    CLOCK: [2020-04-12 Sun 11:36]--[2020-04-12 Sun 12:38] =>  1:02
    CLOCK: [2020-04-11 Sat 19:19]--[2020-04-11 Sat 20:20] =>  1:01
    CLOCK: [2020-04-09 Thu 23:31]--[2020-04-09 Thu 3:59] =>  0:28
    CLOCK: [2020-04-08 Wed 19:24]--[2020-04-08 Wed 20:44] =>  1:20
    :END:
    - [X] Fixing the Kubernetes Cluster (Kris Nova)                                   ( 1h )
    - [X] Address Space Isolation in Linux Kernel (James Bottomley, Mike Rapoport)     ( 1h )
    - [X] Guix (Ludovic Courtes)                                                       ( 1h )
    - [X] How FOSS could revolutionize...(Danese Cooper)                               ( 1h )
    - [X] The selfish contributor explained (James Bottomley)                          ( 1h )
    - [X] The Ethics behind your IOT (Molly de Blanc)                                  ( 1h )
    - [X] HTTP/3 for everyone (Daniel Stenberg)                                        ( 1h )
    - [X] SCION                                                                        ( 1h )
** sandeepk
*** DONE Book Fluent Python Part II [4/4]
    :CLOCKED: [2020-04-23 Thu 09:30]
    :PROPERTIES:
    :ESTIMATED: 14
    :ACTUAL:   10.65
    :OWNER: sandeepk
    :ID: READ.1585286321
    :TASKID: READ.1585286321
    :END:
    :LOGBOOK:
    CLOCK: [2020-04-23 Thu 08:30]--[2020-04-23 Thu 09:30] =>  1:00
    CLOCK: [2020-04-21 Tue 09:50]--[2020-04-21 Tue 10:11] =>  0:21
    CLOCK: [2020-04-21 Tue 08:45]--[2020-04-21 Tue 09:30] =>  0:45
    CLOCK: [2020-04-20 Mon 09:27]--[2020-04-20 Mon 10:20] =>  0:53
    CLOCK: [2020-04-20 Mon 08:39]--[2020-04-20 Mon 08:50] =>  0:11
    CLOCK: [2020-04-17 Fri 09:00]--[2020-04-17 Fri 09:35] =>  0:35
    CLOCK: [2020-04-16 Thu 09:10]--[2020-04-16 Thu 09:45] =>  0:35
    CLOCK: [2020-04-15 Wed 08:40]--[2020-04-15 Wed 09:40] =>  1:00
    CLOCK: [2020-04-14 Tue 08:40]--[2020-04-14 Tue 09:40] =>  1:00
    CLOCK: [2020-04-13 Mon 08:50]--[2020-04-13 Mon 09:50] =>  1:00
    CLOCK: [2020-04-11 Sat 15:05]--[2020-04-11 Sat 16:08] =>  1:04
    CLOCK: [2020-04-11 Sat 13:15]--[2020-04-11 Sat 14:10] =>  0:55
    CLOCK: [2020-04-10 Fri 08:44]--[2020-04-10 Fri 09:40] =>  0:56
    CLOCK: [2020-04-09 Thu 10:00]--[2020-04-09 Thu 10:25] =>  0:25
    :END:
    - [X] Chapter 7. Function Decorators and Closures              ( 3 hr )
    - [X] Chapter 8. Object References, Mutability, and Recycling  ( 4 hr )
    - [X] Chapter 9. A Pythonic Object                             ( 3 hr )
    - [X] Chapter 10. Sequence Hacking, Hashing, and Slicing       ( 4 hr )
*** Write a blog post Part II [/]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:
    :OWNER: sandeepk
    :ID: WRITE.1585286399
    :TASKID: WRITE.1585286399
    :END:
    - [ ] Merge Function as object blog posts into one.  ( 2 hr )

