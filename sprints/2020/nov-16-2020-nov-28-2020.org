#+TITLE: November 16, 2020 - November 28, 2020 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2020-11-16 Mon>
  :wpd-bhavin192: 1
  :END:
** bhavin192
*** DONE Site Reliability Engineering - III. Practices - Part V [1/1]
    CLOSED: [2020-11-23 Mon 22:18]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   5.42
    :OWNER:    bhavin192
    :ID:       READ.1596455869
    :TASKID:   READ.1596455869
    :END:
    :LOGBOOK:
    CLOCK: [2020-11-23 Mon 22:11]--[2020-11-23 Mon 22:18] =>  0:07
    CLOCK: [2020-11-23 Mon 20:57]--[2020-11-23 Mon 21:10] =>  0:13
    CLOCK: [2020-11-23 Mon 20:17]--[2020-11-23 Mon 20:57] =>  0:40
    CLOCK: [2020-11-22 Sun 22:39]--[2020-11-22 Sun 22:58] =>  0:19
    CLOCK: [2020-11-22 Sun 21:53]--[2020-11-22 Sun 22:20] =>  0:27
    CLOCK: [2020-11-22 Sun 20:22]--[2020-11-22 Sun 20:50] =>  0:28
    CLOCK: [2020-11-22 Sun 19:43]--[2020-11-22 Sun 20:11] =>  0:28
    CLOCK: [2020-11-22 Sun 19:17]--[2020-11-22 Sun 19:38] =>  0:21
    CLOCK: [2020-11-19 Thu 22:21]--[2020-11-19 Thu 23:11] =>  0:50
    CLOCK: [2020-11-18 Wed 22:26]--[2020-11-18 Wed 23:11] =>  0:45
    CLOCK: [2020-11-18 Wed 21:06]--[2020-11-18 Wed 21:20] =>  0:14
    CLOCK: [2020-11-17 Tue 22:40]--[2020-11-17 Tue 23:13] =>  0:33
    :END:
    https://landing.google.com/sre/sre-book/toc/index.html
    - [X] 17. Testing for Reliability - Part II                                    (5h)
