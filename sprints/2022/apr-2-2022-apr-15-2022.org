#+TITLE: April 2-15, 2022 (14 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 14
  :SPRINTSTART: <2022-04-02 Sat>
  :wpd-akshay196: 1
  :END:
** akshay196
*** DONE Programming Kubernetes - Part VI
    CLOSED: [2022-04-10 Sun 21:45]
    :PROPERTIES:
    :ESTIMATED:   9
    :ACTUAL:   6.07
    :OWNER: akshay196
    :ID: READ.1637477677
    :TASKID: READ.1637477677
    :END:
    :LOGBOOK:
    CLOCK: [2022-04-10 Sun 21:00]--[2022-04-10 Sun 21:45] =>  0:45
    CLOCK: [2022-04-07 Thu 21:02]--[2022-04-07 Thu 21:34] =>  0:32
    CLOCK: [2022-04-06 Wed 20:02]--[2022-04-06 Wed 21:00] =>  0:58
    CLOCK: [2022-04-05 Tue 20:12]--[2022-04-05 Tue 21:25] =>  1:13
    CLOCK: [2022-04-04 Mon 20:50]--[2022-04-04 Mon 21:48] =>  0:58
    CLOCK: [2022-04-03 Sun 20:25]--[2022-04-03 Sun 20:57] =>  0:32
    CLOCK: [2022-04-02 Sat 19:55]--[2022-04-02 Sat 21:01] =>  1:06
    :END:
    - [X] Chapter 6. Solutions for Writing Operators     (4h)
    - [X] Chapter 7. Shipping Controllers and Operators  (5h)
*** DONE Watch Fosdem 2022 videos - Part II
    CLOSED: [2022-04-15 Fri 21:48]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   2.35
    :OWNER: akshay196
    :ID: READ.1645157068
    :TASKID: READ.1645157068
    :END:
    :LOGBOOK:
    CLOCK: [2022-04-15 Fri 20:50]--[2022-04-15 Fri 21:48] =>  0:58
    CLOCK: [2022-04-15 Fri 14:58]--[2022-04-15 Fri 15:28] =>  0:30
    CLOCK: [2022-04-15 Fri 11:35]--[2022-04-15 Fri 11:54] =>  0:19
    CLOCK: [2022-04-11 Mon 22:15]--[2022-04-11 Mon 22:49] =>  0:34
    :END:
    - [X]  [[https://fosdem.org/2022/schedule/event/postgresql_exploring_linux_memory_usage_and_io_performance_for_cloud_native_databases/][Exploring Linux Memory Usage and IO Performance for Cloud Native Databases]]                                         (45m)
    - [X]  [[https://fosdem.org/2022/schedule/event/python_concurrency_in_webapps/][Handling Concurrency in Web Application How *not* to build a URL Shortener]]                                         (45m)
    - [X]  [[https://fosdem.org/2022/schedule/event/python_unicode/][Messing with unicode A few possible attacks with unicode]]                                                           (45m)
