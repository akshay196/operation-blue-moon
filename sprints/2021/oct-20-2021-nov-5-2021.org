#+TITLE: October 20-November 5, 2021 (17 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 17
  :SPRINTSTART: <2021-10-20 Wed>
  :wpd-akshay196: 1
  :wpd-bhavin192: 2
  :wpd-nihaal: 1
  :END:
** akshay196
*** DONE Let's Go Book - Part IV
    CLOSED: [2021-10-29 Fri 16:34]
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:   8.03
    :OWNER:    akshay196
    :ID:       READ.1629798238
    :TASKID:   READ.1629798238
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-29 Fri 16:19]--[2021-10-29 Fri 16:34] =>  0:15
    CLOCK: [2021-10-27 Wed 19:56]--[2021-10-27 Wed 20:52] =>  0:56
    CLOCK: [2021-10-26 Tue 19:01]--[2021-10-26 Tue 19:55] =>  0:54
    CLOCK: [2021-10-25 Mon 20:13]--[2021-10-25 Mon 21:24] =>  1:11
    CLOCK: [2021-10-24 Sun 15:18]--[2021-10-24 Sun 16:12] =>  0:54
    CLOCK: [2021-10-23 Sat 18:31]--[2021-10-23 Sat 19:03] =>  0:32
    CLOCK: [2021-10-22 Fri 18:33]--[2021-10-22 Fri 19:48] =>  1:15
    CLOCK: [2021-10-21 Thu 19:27]--[2021-10-21 Thu 20:31] =>  1:04
    CLOCK: [2021-10-20 Wed 19:12]--[2021-10-20 Wed 20:13] =>  1:01
    :END:
    - [X] 4.4. Designing a Database Model            (1h)
    - [X] 4.5. Executing SQL Statements              (1h)
    - [X] 4.6. Single-record SQL Queries             (1h)
    - [X] 4.7. Multiple-record SQL Queries           (1h)
    - [X] 4.8. Transactions and Other Details        (90m)
    - [X] 5.1. Displaying Dynamic Data               (90m)
    - [X] 5.2. Template Actions and Functions        (1h)
    - [X] 5.3. Caching Templates                     (1h)
    - [X] 5.4. Catching Runtime Errors               (1h)
*** DONE Read Kubernetes docs - Part VI
    CLOSED: [2021-11-02 Tue 19:51]
    :PROPERTIES:
    :ESTIMATED: 7
    :ACTUAL:   2.03
    :OWNER:    akshay196
    :ID:       READ.1623432379
    :TASKID:   READ.1623432379
    :END:
    :LOGBOOK:
    CLOCK: [2021-11-02 Tue 18:55]--[2021-11-02 Tue 19:51] =>  0:56
    CLOCK: [2021-11-01 Mon 19:04]--[2021-11-01 Mon 19:36] =>  0:32
    CLOCK: [2021-10-29 Fri 20:42]--[2021-10-29 Fri 21:16] =>  0:34
    :END:
    https://kubernetes.io/docs
    - [X] Persistent Volumes                     (2h)
    - [X] Volume Snapshots                       (3h)
    - [X] CSI Volume Cloning                     (2h)
** bhavin192
*** DONE CKS exam preparation [2/2]
    CLOSED: [2021-10-28 Thu 10:31]
    :PROPERTIES:
    :ESTIMATED: 34
    :ACTUAL:   30.42
    :OWNER:    bhavin192
    :ID:       READ.1636906976
    :TASKID:   READ.1636906976
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-28 Thu 07:58]--[2021-10-28 Thu 10:31] =>  2:33
    CLOCK: [2021-10-27 Wed 22:02]--[2021-10-27 Wed 23:35] =>  1:33
    CLOCK: [2021-10-27 Wed 19:35]--[2021-10-27 Wed 21:37] =>  2:02
    CLOCK: [2021-10-27 Wed 19:10]--[2021-10-27 Wed 19:25] =>  0:15
    CLOCK: [2021-10-27 Wed 18:42]--[2021-10-27 Wed 19:03] =>  0:21
    CLOCK: [2021-10-26 Tue 21:55]--[2021-10-27 Wed 00:09] =>  2:14
    CLOCK: [2021-10-26 Tue 19:47]--[2021-10-26 Tue 21:15] =>  1:28
    CLOCK: [2021-10-25 Mon 23:04]--[2021-10-25 Mon 23:34] =>  0:30
    CLOCK: [2021-10-25 Mon 22:14]--[2021-10-25 Mon 22:55] =>  0:41
    CLOCK: [2021-10-25 Mon 20:14]--[2021-10-25 Mon 21:14] =>  1:00
    CLOCK: [2021-10-24 Sun 22:43]--[2021-10-24 Sun 22:56] =>  0:13
    CLOCK: [2021-10-24 Sun 22:20]--[2021-10-24 Sun 22:33] =>  0:13
    CLOCK: [2021-10-24 Sun 19:47]--[2021-10-24 Sun 20:26] =>  0:39
    CLOCK: [2021-10-24 Sun 19:17]--[2021-10-24 Sun 19:30] =>  0:13
    CLOCK: [2021-10-24 Sun 18:32]--[2021-10-24 Sun 18:55] =>  0:23
    CLOCK: [2021-10-24 Sun 16:22]--[2021-10-24 Sun 17:45] =>  1:23
    CLOCK: [2021-10-24 Sun 15:40]--[2021-10-24 Sun 16:10] =>  0:30
    CLOCK: [2021-10-24 Sun 14:13]--[2021-10-24 Sun 15:05] =>  0:52
    CLOCK: [2021-10-24 Sun 13:33]--[2021-10-24 Sun 14:01] =>  0:28
    CLOCK: [2021-10-24 Sun 12:10]--[2021-10-24 Sun 13:15] =>  1:05
    CLOCK: [2021-10-23 Sat 23:00]--[2021-10-23 Sat 23:49] =>  0:49
    CLOCK: [2021-10-23 Sat 22:05]--[2021-10-23 Sat 22:30] =>  0:25
    CLOCK: [2021-10-23 Sat 20:01]--[2021-10-23 Sat 21:11] =>  1:10
    CLOCK: [2021-10-23 Sat 17:50]--[2021-10-23 Sat 18:59] =>  1:09
    CLOCK: [2021-10-23 Sat 16:15]--[2021-10-23 Sat 17:22] =>  1:07
    CLOCK: [2021-10-23 Sat 13:04]--[2021-10-23 Sat 13:29] =>  0:25
    CLOCK: [2021-10-23 Sat 11:58]--[2021-10-23 Sat 12:53] =>  0:55
    CLOCK: [2021-10-22 Fri 22:04]--[2021-10-22 Fri 23:51] =>  1:47
    CLOCK: [2021-10-22 Fri 20:52]--[2021-10-22 Fri 21:16] =>  0:24
    CLOCK: [2021-10-22 Fri 08:15]--[2021-10-22 Fri 08:35] =>  0:20
    CLOCK: [2021-10-21 Thu 21:55]--[2021-10-21 Thu 22:50] =>  0:55
    CLOCK: [2021-10-21 Thu 20:02]--[2021-10-21 Thu 21:21] =>  1:19
    CLOCK: [2021-10-20 Wed 23:07]--[2021-10-20 Wed 23:37] =>  0:30
    CLOCK: [2021-10-20 Wed 21:33]--[2021-10-20 Wed 21:46] =>  0:13
    CLOCK: [2021-10-20 Wed 21:13]--[2021-10-20 Wed 21:29] =>  0:16
    CLOCK: [2021-10-20 Wed 21:04]--[2021-10-20 Wed 21:09] =>  0:05
    :END:
    - [X] [[https://www.udemy.com/course/certified-kubernetes-security-specialist/][Kubernetes CKS 2021 Complete Course - Theory - Practice]]       (32h)
    - [X] [[https://www.cncf.io/certification/cks/][Certified Kubernetes Security Specialist (CKS)]] exam           ( 2h)
** nihaal
*** DONE Write blog posts
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   10.77
    :OWNER: nihaal
    :ID: WRITE.1634660229
    :TASKID: WRITE.1634660229
    :END:
    :LOGBOOK:
    CLOCK: [2021-11-06 Sat 18:13]--[2021-11-06 Sat 19:11] =>  0:58
    CLOCK: [2021-11-05 Fri 20:08]--[2021-11-05 Fri 21:00] =>  0:52
    CLOCK: [2021-11-05 Fri 18:35]--[2021-11-05 Fri 19:16] =>  0:41
    CLOCK: [2021-11-05 Fri 18:11]--[2021-11-05 Fri 18:31] =>  0:20
    CLOCK: [2021-11-03 Wed 20:51]--[2021-11-03 Wed 21:09] =>  0:18
    CLOCK: [2021-11-03 Wed 18:33]--[2021-11-03 Wed 19:55] =>  1:22
    CLOCK: [2021-11-01 Mon 20:35]--[2021-11-01 Mon 21:26] =>  0:51
    CLOCK: [2021-10-30 Sat 21:52]--[2021-10-30 Sat 23:28] =>  1:36
    CLOCK: [2021-10-30 Sat 09:43]--[2021-10-30 Sat 09:49] =>  0:06
    CLOCK: [2021-10-29 Fri 18:16]--[2021-10-29 Fri 19:58] =>  1:42
    CLOCK: [2021-10-27 Wed 18:27]--[2021-10-27 Wed 19:59] =>  1:32
    CLOCK: [2021-10-24 Sun 18:25]--[2021-10-24 Sun 18:53] =>  0:28
    :END:
    - [X] Creating Misc Character devices     (3h)
    - [X] Creating Debugfs entries            (3h)
*** DONE Read Linux Device Drivers, 3rd edition - Part II
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   2.00
    :OWNER: nihaal
    :ID: READ.1632069861
    :TASKID: READ.1632069861
    :END:
    :LOGBOOK:
    CLOCK: [2021-11-02 Tue 20:25]--[2021-11-02 Tue 21:04] =>  0:39
    CLOCK: [2021-11-02 Tue 19:09]--[2021-11-02 Tue 20:00] =>  0:51
    CLOCK: [2021-11-02 Tue 18:35]--[2021-11-02 Tue 19:05] =>  0:30
    :END:
     - [X] 4. Debugging Techniques            (3h)
*** DONE Eudyptula Challenge - Part II
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   1.40
    :OWNER: nihaal
    :ID: DEV.1632240155
    :TASKID: DEV.1632240155
    :END:
    :LOGBOOK:
    CLOCK: [2021-11-01 Mon 18:19]--[2021-11-01 Mon 19:43] =>  1:24
    :END:
    - [X] 9. Creating sysfs entries           (3h)
*** DONE Read Linux Weekly News
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   2.92
    :OWNER: nihaal
    :ID: READ.1632238538
    :TASKID: READ.1632238538
    :END:
    :LOGBOOK:
    CLOCK: [2021-11-04 Thu 18:16]--[2021-11-04 Thu 19:11] =>  0:55
    CLOCK: [2021-10-28 Thu 18:27]--[2021-10-28 Thu 19:29] =>  1:02
    CLOCK: [2021-10-23 Sat 18:14]--[2021-10-23 Sat 19:12] =>  0:58
    :END:
    - [X] Weekly edition for October 14, 2021 (1h)
    - [X] Weekly edition for October 21, 2021 (1h)
    - [X] Weekly edition for October 28, 2021 (1h)
